%global _empty_manifest_terminate_build 0
Name:		python-pytools
Version:	2023.1.1
Release:	1
Summary:	A collection of tools for Python
License:	MIT
URL:		http://pypi.python.org/pypi/pytools
Source0:	https://files.pythonhosted.org/packages/02/a0/e4bba94fdbdd7ee5b46a954e6ecdce98ee8ba167a02d82599fc0d5c1f97f/pytools-2023.1.1.tar.gz
BuildArch:	noarch

%description
Pytools is a big bag of things that are "missing" from the Python standard library.

%package -n python3-pytools
Summary:	A collection of tools for Python
Provides:	python-pytools
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-pytools
Pytools is a big bag of things that are "missing" from the Python standard library.

%package help
Summary:	Development documents and examples for pytools
Provides:	python3-pytools-doc

%description help
Pytools is a big bag of things that are "missing" from the Python standard library.

%prep
%autosetup -n pytools-2023.1.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pytools -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jul 28 2023 sunhui <sunhui@kylinos.cn> - 2023.1.1-1
- Update package to version 2023.1.1

* Fri Jul 16 2021 OpenStack_SIG <openstack@openeuler.org>
- Package Spec generated
